//https://www.youtube.com/watch?v=76nZ9q_BUn8

import { Component } from '@angular/core';
import { Http, Response} from '@angular/http'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  cityName="";
  cityHumidity="";
  cityDescription ="";

  constructor (private http:Http){

  }

  searchCity(){
    if  (this.cityName.toString().length > 0) {
      this.http.get("http://api.openweathermap.org/data/2.5/weather?q="+ this.cityName + "&appid=b1d018f7af13c89e2a33843b9bc15195" )
      .subscribe (
       (res: Response) => {
            const weatherCity= res.json();
            console.log(weatherCity);
            this.cityHumidity = weatherCity.main.humidity;
            this.cityDescription = weatherCity.weather[0].main;
          }
      )
    }
  }
    
}
